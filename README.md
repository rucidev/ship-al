# Ship.al
-------
This is the project for "Albanian Skills 5 Software Development Competition". It is implemented in Java Programming Language.


## Project Structure
-------
Source code is located in **src.main.java.al.ship.core:**

* package model  
	* contains POJO classes
* package util 
	* constains a Utilities class
* package io 
	* contains classes that deal with Input/Output operations for text files and JSON files
* package algorithm 
	* contains classes that implement various algorithms

Resource files including the JSON file and text files serving as an existing database of data
and other text files with all test inputs for the algorithms and their corresponding outputs are located in
**src.main.resources**.

Test classes are located in **src.test.java.al.ship.core**. 

## Checking The Solutions
-------
For every problem statement there is a test class which compares the correct results read from the resources folder
to the output provided by the solution of the competitor. This test class calculates the total number of test cases 
passed, shows the results of the cases that did not pass the test and calculates the total time in milliseconds of 
the provided solution to compute the result.

In order to run these tests all you need to do is **run src.test.java.al.ship.core.ASTests** and the
results will appear in console.

## Competition
-------
The competitior will be provided with the facility of having all the IO operations ready(except from reading from a 
JSON file) so that he/she can focus on solving the problems. There are 3 TODO comments in the project that will give
competitors bonus points if completed. In addition, the models will be provided with their full implementation as well.

The competitor can use the test classes and the test files(with only a partion of all the test cases) to test his/her 
solution. These files are found in **src.main.resources.input.test** and **src.main.resources.output.test**.