package al.ship.core;

import al.ship.core.constest.*;
import org.junit.Rule;
import org.junit.runner.JUnitCore;

/**
 * @author aleksruci on 29/Apr/2019
 */
public class ASTests {
    protected int nrTests;
    protected int nrPassed = 0;

    @Rule
    public JUnitStopWatch stopwatch = new JUnitStopWatch();

    public static void main(String[] args) {
        new JUnitCore().run(
                Problem1Test.class,
                Problem2Test.class,
                Problem3Test.class,
                Problem4Test.class,
                Problem5Test.class,
                Problem6Test.class,
                Problem7Test.class,
                Problem8Test.class);
    }

    protected String getTestErrorMessage(int testNr, AssertionError ex) {
        return "Test " + testNr + " " + ex.getMessage();
    }

    protected String getIOErrorMessage(int problemId) {
        return"[Problem " + problemId + "]: Number of inputs should be equal to number of outputs ";
    }
}