package al.ship.core.constest;

import al.ship.core.ASResult;
import al.ship.core.ASTests;
import al.ship.core.algorithm.WorkingDaysAlgorithm;
import al.ship.core.io.InputFileLayer;
import al.ship.core.io.OutputFileLayer;
import al.ship.core.io.ResourceFileLayer;
import al.ship.core.model.DateInterval;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author aleksruci on 29/Apr/2019
 */
public class Problem2Test extends ASTests {
    public static int PROBLEM_ID = 2;

    private List<Calendar> holidays;
    private List<Calendar> workingSaturdays;
    private List<DateInterval> intervals;

    private List<Integer> results;

    @Before
    public void readFiles() throws IOException, ParseException {
        holidays = new ResourceFileLayer().getHolidays();
        workingSaturdays = new ResourceFileLayer().getWorkingSaturdays();

//        intervals = new InputFileLayer().getDateIntervalsTest();
//        results = new OutputFileLayer().getDateIntervalsOutputTest();

        intervals = new InputFileLayer().getDateIntervals();
        results = new OutputFileLayer().getDateIntervalsOutput();

        try {
            assertEquals(getIOErrorMessage(PROBLEM_ID), intervals.size(), results.size(), 0);
        } catch (AssertionError ex) {
            System.out.println(ex.getMessage());
        }
        nrTests = intervals.size(); // set number of tests
    }

    @Test
    public void shouldReturnNumberOfWorkingDays() {
        ASResult res = new ASResult(PROBLEM_ID, nrTests);

        WorkingDaysAlgorithm algorithm = new WorkingDaysAlgorithm(holidays, workingSaturdays);
        for (int i = 0; i < nrTests; i++) {
            try {
                Date start = intervals.get(i).getStart();
                Date end = intervals.get(i).getEnd();

                assertEquals(results.get(i), algorithm.getNumberOfWorkingDays(start, end), 0);
                nrPassed++;
            } catch (AssertionError ex) {
                res.addErrorMsg(getTestErrorMessage(i, ex));
            }
        }
        res.setNrPassed(nrPassed);
        System.out.print(res.getDescription());
    }

}

