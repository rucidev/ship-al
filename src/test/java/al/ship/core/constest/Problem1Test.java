package al.ship.core.constest;

import al.ship.core.ASTests;
import al.ship.core.ASResult;
import al.ship.core.io.InputFileLayer;
import al.ship.core.io.OutputFileLayer;
import al.ship.core.model.shipment.Shipment;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static al.ship.core.algorithm.DiscountAlgorithms.applyBestOrder3Pay2Deal;
import static org.junit.Assert.assertEquals;

/**
 * @author aleksruci on 29/Apr/2019
 */
public class Problem1Test extends ASTests {
    public static int PROBLEM_ID = 1;

    private List<Shipment> shipments;

    private List<Double> results;

    @Before
    public void readFiles() throws IOException {
//        shipments = new InputFileLayer().getShipmentsTest();
//        results = new OutputFileLayer().getShipmentsOutputTest();

        shipments = new InputFileLayer().getShipments();
        results = new OutputFileLayer().getShipmentsOutput();

        try {
            assertEquals(getIOErrorMessage(PROBLEM_ID), shipments.size(), results.size(), 0);
        } catch (AssertionError ex) {
            System.out.println(ex.getMessage());
        }

        nrTests = shipments.size(); // set number of tests
    }

    @Test
    public void shouldReturnBestOrder3Pay2Deal() {
        ASResult res = new ASResult(PROBLEM_ID, nrTests);
        for (int i = 0; i < nrTests; i++) {
            try {
                applyBestOrder3Pay2Deal(shipments.get(i));

                assertEquals(results.get(i), shipments.get(i).getPrice(), 0.001);
                nrPassed++;
            } catch (AssertionError ex) {
                res.addErrorMsg(getTestErrorMessage(i, ex));
            }
        }
        res.setNrPassed(nrPassed);
        System.out.print(res.getDescription());
    }
}
