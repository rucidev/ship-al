package al.ship.core.constest;

import al.ship.core.ASResult;
import al.ship.core.ASTests;
import al.ship.core.io.InputFileLayer;
import al.ship.core.io.OutputFileLayer;
import al.ship.core.model.transport.StationPair;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static al.ship.core.algorithm.TransportAlgorithms.getShortestPathLength;
import static org.junit.Assert.assertEquals;

/**
 * @author aleksruci on 30/Apr/2019
 */
public class Problem7Test extends ASTests {
    public static int PROBLEM_ID = 7;

    private List<StationPair> pairs;

    private List<Double> results;

    @Before
    public void readFiles() throws IOException {
//        pairs = new InputFileLayer().getStationPairsTest();
//        results = new OutputFileLayer().getStationPairsOutputTest();

        pairs = new InputFileLayer().getStationPairs();
        results = new OutputFileLayer().getStationPairsOutput();

        try {
            assertEquals(getIOErrorMessage(PROBLEM_ID), pairs.size(), results.size(), 0);
        } catch (AssertionError ex) {
            System.out.println(ex.getMessage());
        }

        nrTests = pairs.size(); // set number of tests
    }

    @Test
    public void shouldFindLengthOfShortestPath() {
        ASResult res = new ASResult(PROBLEM_ID, nrTests);

        for (int i = 0; i < nrTests; i++) {
            try {
                assertEquals(results.get(i), getShortestPathLength(pairs.get(i).getStart(), pairs.get(i).getEnd()), 0.001);
                nrPassed++;
            } catch (AssertionError ex) {
                res.addErrorMsg(getTestErrorMessage(i, ex));
            }
        }
        res.setNrPassed(nrPassed);
        System.out.print(res.getDescription());
    }
}
