package al.ship.core.constest;

import al.ship.core.ASResult;
import al.ship.core.ASTests;
import al.ship.core.io.InputFileLayer;
import al.ship.core.io.OutputFileLayer;
import al.ship.core.model.CustomEmail;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static al.ship.core.algorithm.EmailAlgorithms.getEmailFromTemplate;
import static org.junit.Assert.assertEquals;

/**
 * @author aleksruci on 30/Apr/2019
 */
public class Problem5Test extends ASTests {
    public static int PROBLEM_ID = 5;

    private List<CustomEmail> emails;

    private List<String> results;

    @Before
    public void readFiles() throws IOException {
//        emails = new InputFileLayer().getCustomEmailsTest();
//        results = new OutputFileLayer().getCustomEmailsOutputTest();

        emails = new InputFileLayer().getCustomEmails();
        results = new OutputFileLayer().getCustomEmailsOutput();

        try {
            assertEquals(getIOErrorMessage(PROBLEM_ID), emails.size(), results.size(), 0);
        } catch (AssertionError ex) {
            System.out.println(ex.getMessage());
        }

        nrTests = emails.size(); // set number of tests
    }

    @Test
    public void shouldCreateEmailFromTemplate() {
        ASResult res = new ASResult(PROBLEM_ID, nrTests);

        for (int i = 0; i < nrTests; i++) {
            try {
                assertEquals(results.get(i), getEmailFromTemplate(emails.get(i)));
                nrPassed++;
            } catch (AssertionError ex) {
                res.addErrorMsg(getTestErrorMessage(i, ex));
            }
        }
        res.setNrPassed(nrPassed);
        System.out.print(res.getDescription());
    }

}
