package al.ship.core.constest;

import al.ship.core.ASResult;
import al.ship.core.ASTests;
import al.ship.core.algorithm.UserNetwork;
import al.ship.core.io.InputFileLayer;
import al.ship.core.io.OutputFileLayer;
import al.ship.core.model.user.UserPair;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author aleksruci on 29/Apr/2019
 */
public class Problem4Test extends ASTests {
    public static int PROBLEM_ID = 4;

    private List<List<UserPair>> pairsMatrix;

    private List<Integer> results;

    @Before
    public void readFiles() throws IOException {
//        pairsMatrix = new InputFileLayer().getUserPairsTest();
//        results = new OutputFileLayer().getUserPairsOutputTest();

        pairsMatrix = new InputFileLayer().getUserPairs();
        results = new OutputFileLayer().getUserPairsOutput();

        try {
            assertEquals(getIOErrorMessage(PROBLEM_ID), pairsMatrix.size(), results.size(), 0);
        } catch (AssertionError ex) {
            System.out.println(ex.getMessage());
        }

        nrTests = pairsMatrix.size(); // set number of tests
    }

    @Test
    public void shouldFindSizeOfLargestNetwork() {
        ASResult res = new ASResult(PROBLEM_ID, nrTests);

        for (int i = 0; i < nrTests; i++) {
            try {
                UserNetwork network = new UserNetwork();
                List<UserPair> pairs = pairsMatrix.get(i);

                for (int j = 0; j < pairs.size(); j++) {
                    UserPair userPair = pairs.get(j);
                    network.union(userPair.getUser1(), userPair.getUser2());
                }

                assertEquals(results.get(i), network.getMaxConnectionLength(), 0);
                nrPassed++;
            } catch (AssertionError ex) {
                res.addErrorMsg(getTestErrorMessage(i, ex));
            }
        }
        res.setNrPassed(nrPassed);
        System.out.print(res.getDescription());
    }

}
