package al.ship.core.constest;

import al.ship.core.ASResult;
import al.ship.core.ASTests;
import al.ship.core.algorithm.TimeOverlapAlgorithms;
import al.ship.core.io.InputFileLayer;
import al.ship.core.io.JSONLayer;
import al.ship.core.io.OutputFileLayer;
import al.ship.core.model.driver.DriverWorkingHours;
import al.ship.core.model.driver.HoursInterval;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * @author aleksruci on 29/Apr/2019
 */
public class Problem3Test extends ASTests {
    public static int PROBLEM_ID = 3;

    private Map<Integer, DriverWorkingHours> workingHours;
    private List<HoursInterval> hoursIntervals;

    private List<String> results;

    @Before
    public void readFiles() throws IOException {
        workingHours = new JSONLayer().getDriversWorkingHours();

//        hoursIntervals = new InputFileLayer().getHourIntervalsTest();
//        results = new OutputFileLayer().getHourIntervalsOutputTest();

        hoursIntervals = new InputFileLayer().getHourIntervals();
        results = new OutputFileLayer().getHourIntervalsOutput();

        try {
            assertEquals(getIOErrorMessage(PROBLEM_ID), hoursIntervals.size(), results.size(), 0);
        } catch (AssertionError ex) {
            System.out.println(ex.getMessage());
        }

        nrTests = hoursIntervals.size(); // set number of tests
    }

    @Test
    public void shouldDecideIfOverlapOccurs() {
        ASResult res = new ASResult(PROBLEM_ID, nrTests);

        for (int i = 0; i < nrTests; i++) {
            try {
                HoursInterval interval = hoursIntervals.get(i);

                Assert.assertEquals(results.get(i).equals("YES"), TimeOverlapAlgorithms.hasOverlap(interval, workingHours.get(interval.getId())));
                nrPassed++;
            } catch (AssertionError ex) {
                res.addErrorMsg(getTestErrorMessage(i, ex));
            }
        }
        res.setNrPassed(nrPassed);
        System.out.print(res.getDescription());
    }
}
