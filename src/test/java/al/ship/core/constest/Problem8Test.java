package al.ship.core.constest;

import al.ship.core.ASResult;
import al.ship.core.ASTests;
import al.ship.core.io.InputFileLayer;
import al.ship.core.io.OutputFileLayer;
import al.ship.core.model.transport.Transportation;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static al.ship.core.algorithm.TransportAlgorithms.getMaxWeightInShips;
import static org.junit.Assert.assertEquals;

/**
 * @author aleksruci on 30/Apr/2019
 */
public class Problem8Test extends ASTests {
    public static int PROBLEM_ID = 8;

    private List<Transportation> transportations;

    private List<Double> results;

    @Before
    public void readFiles() throws IOException {
//        transportations = new InputFileLayer().getTransportationTest();
//        results = new OutputFileLayer().getTransportationOutputTest();

        transportations = new InputFileLayer().getTransportation();
        results = new OutputFileLayer().getTransportationOutput();

        try {
            assertEquals(getIOErrorMessage(PROBLEM_ID), transportations.size(), results.size(), 0);
        } catch (AssertionError ex) {
            System.out.println(ex.getMessage());
        }

        nrTests = transportations.size(); // set number of tests
    }

    @Test
    public void shouldFindMaxWeightInShips() {
        ASResult res = new ASResult(PROBLEM_ID, nrTests);

        for (int i = 0; i < nrTests; i++) {
            try {
                assertEquals(results.get(i), getMaxWeightInShips(transportations.get(i)), 0.001);
                nrPassed++;
            } catch (AssertionError ex) {
                res.addErrorMsg(getTestErrorMessage(i, ex));
            }
        }
        res.setNrPassed(nrPassed);
        System.out.print(res.getDescription());
    }
}
