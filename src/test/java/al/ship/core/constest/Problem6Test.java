package al.ship.core.constest;

import al.ship.core.ASResult;
import al.ship.core.ASTests;
import al.ship.core.io.InputFileLayer;
import al.ship.core.io.OutputFileLayer;
import al.ship.core.model.shipment.ShipCode;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author aleksruci on 30/Apr/2019
 */
public class Problem6Test extends ASTests {
    public static int PROBLEM_ID = 6;

    private List<ShipCode> shipCodes;

    private List<Integer> results;

    @Before
    public void readFiles() throws IOException {
//        shipCodes = new InputFileLayer().getShipCodesTest();
//        results = new OutputFileLayer().getShipCodesOutputTest();

        shipCodes = new InputFileLayer().getShipCodes();
        results = new OutputFileLayer().getShipCodesOutput();

        try {
            assertEquals(getIOErrorMessage(PROBLEM_ID), shipCodes.size(), results.size(), 0);
        } catch (AssertionError ex) {
            System.out.println(ex.getMessage());
        }

        nrTests = shipCodes.size(); // set number of tests
    }

    @Test
    public void shouldFindOrderIdFromShipCode() {
        ASResult res = new ASResult(PROBLEM_ID, nrTests);

        for (int i = 0; i < nrTests; i++) {
            try {
                assertEquals(results.get(i), shipCodes.get(i).getOrderId(), 0);
                nrPassed++;
            } catch (AssertionError ex) {
                res.addErrorMsg(getTestErrorMessage(i, ex));
            }
        }
        res.setNrPassed(nrPassed);
        System.out.print(res.getDescription());
    }
}
