package al.ship.core;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * @author aleksruci on 30/Apr/2019
 */
public class ASResult {
    private int problemId;
    private int nrTests;
    private int nrPassed;
    private List<String> errorMsgs;

    public ASResult(int problemId, int nrTests) {
        this.problemId = problemId;
        this.nrTests = nrTests;
        errorMsgs = new ArrayList<>();
    }

    public double getPercentage() {
        return BigDecimal.valueOf(100.0 * nrPassed / nrTests)
                .setScale(2, RoundingMode.HALF_EVEN)
                .doubleValue(); //2 digit precision
    }

    public String getDescription() {
        StringBuilder res = new StringBuilder();
        res.append("[Problem ").append(problemId).append("]:");
        res.append(" Tests passed: ").append(nrPassed).append("/").append(nrTests);
        res.append(" => ").append(getPercentage()).append("% ");
//        for (int i = 0; i < errorMsgs.size() && i < 3; i++) {
//            res.append(errorMsgs.get(i));
//        }
        return res.toString();
    }

    public int getProblemId() {
        return problemId;
    }

    public void setProblemId(int problemId) {
        this.problemId = problemId;
    }

    public void setNrTests(int nrTests) {
        this.nrTests = nrTests;
    }

    public void setNrPassed(int nrPassed) {
        this.nrPassed = nrPassed;
    }

    public List<String> getErrorMsgs() {
        return errorMsgs;
    }

    public void addErrorMsg(String error) {
        this.errorMsgs.add("\n\t\t\t Error: " + error);
    }
}
