package al.ship.core.io;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author aleksruci on 28/Apr/2019
 */
class DefaultIOLayer {
    static final String RESOURCES_PATH = "src/main/resources/";

    /**
     * @param path name of the file to read
     * @return list of every line of the file
     * @throws IOException if IO error occurs
     */
    List<String> readFile(String path) throws IOException {
        return Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
    }

    void writeFile(String path, List<String> lines) throws IOException {
        Files.write(Paths.get(path), lines, StandardCharsets.UTF_8);
    }
}
