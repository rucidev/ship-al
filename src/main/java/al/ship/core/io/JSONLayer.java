package al.ship.core.io;

import al.ship.core.model.driver.DriverWorkingHours;
import al.ship.core.model.driver.HoursInterval;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * @author aleksruci on 28/Apr/2019
 */
public class JSONLayer extends DefaultIOLayer {
    public static final String DRIVER_WORKING_HOURS_PATH = RESOURCES_PATH + "driver_timetable.json";

    private static final String DRIVER_WORKING_HOURS = "driver_working_hours";
    private static final String DRIVER_ID = "driver_id";
    private static final String START_TIME = "start_time";
    private static final String END_TIME = "end_time";
    private static final String WEEKDAY = "weekday";

    public JSONLayer() {

    }

    public Map<Integer, DriverWorkingHours> getDriversWorkingHours() throws IOException {
        String text = new String(Files.readAllBytes(Paths.get(DRIVER_WORKING_HOURS_PATH)), StandardCharsets.UTF_8);
        JSONObject object = new JSONObject(text);
        JSONArray workingHoursJSON = object.getJSONArray(DRIVER_WORKING_HOURS);
        int length = workingHoursJSON.length();

        Map<Integer, DriverWorkingHours> workingHours = new HashMap<>();
        for(int i = 0; i < length; i++) {
            JSONObject driverTime = workingHoursJSON.getJSONObject(i);
            int driverId = driverTime.getInt(DRIVER_ID);

            if (workingHours.get(driverId) == null) {
                workingHours.put(driverId, new DriverWorkingHours(driverId));
            }
            HoursInterval hoursInterval = new HoursInterval();
            hoursInterval.setStartTime(driverTime.getString(START_TIME));
            hoursInterval.setEndTime(driverTime.getString(END_TIME));

            workingHours.get(driverId).addTimeInterval(driverTime.getString(WEEKDAY), hoursInterval);
        }
        return workingHours;
    }
}
