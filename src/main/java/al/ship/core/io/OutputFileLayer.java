package al.ship.core.io;

import al.ship.core.util.Utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author aleksruci on 29/Apr/2019
 */
public class OutputFileLayer extends DefaultIOLayer {
    private static final String OUTPUT_PATH = RESOURCES_PATH + "output/";
    private static final String TEST_OUTPUT_PATH = RESOURCES_PATH + "output/test/";

    private static final String ORDERS_PRICE_FILE_NAME = "orders_price_output.txt";
    private static final String DATES_FILE_NAME = "dates_output.txt";
    private static final String WORKING_HOURS_FILE_NAME = "working_hours_output.txt";
    private static final String NETWORK_FILE_NAME = "network_output.txt";
    private static final String EMAILS_FILE_NAME = "email_output.txt";
    private static final String SHIP_CODES_FILE_NAME = "ship_codes_output.txt";
    private static final String STATIONS_FILE_NAME = "stations_output.txt";
    private static final String ORDERS_WEIGHT_FILE_NAME = "orders_weight_output.txt";

    public List<Double> getShipmentsOutput() throws IOException {
        return getShipmentsOutput(OUTPUT_PATH);
    }

    public List<Double> getShipmentsOutputTest() throws IOException {
        return getShipmentsOutput(TEST_OUTPUT_PATH);
    }

    public List<Integer> getDateIntervalsOutput() throws IOException {
        return getDateIntervalsOutput(OUTPUT_PATH);
    }

    public List<Integer> getDateIntervalsOutputTest() throws IOException {
        return getDateIntervalsOutput(TEST_OUTPUT_PATH);
    }

    public List<String> getHourIntervalsOutput() throws IOException {
        return getHourIntervalsOutput(OUTPUT_PATH);
    }

    public List<String> getHourIntervalsOutputTest() throws IOException {
        return getHourIntervalsOutput(TEST_OUTPUT_PATH);
    }

    public List<Integer> getUserPairsOutput() throws IOException {
        return getUserPairsOutput(OUTPUT_PATH);
    }

    public List<Integer> getUserPairsOutputTest() throws IOException {
        return getUserPairsOutput(TEST_OUTPUT_PATH);
    }

    public List<String> getCustomEmailsOutput() throws IOException {
        return getCustomEmailsOutput(OUTPUT_PATH);
    }

    public List<String> getCustomEmailsOutputTest() throws IOException {
        return getCustomEmailsOutput(TEST_OUTPUT_PATH);
    }

    public List<Integer> getShipCodesOutput() throws IOException {
        return getShipCodesOutput(OUTPUT_PATH);
    }

    public List<Integer> getShipCodesOutputTest() throws IOException {
        return getShipCodesOutput(TEST_OUTPUT_PATH);
    }

    public List<Double> getStationPairsOutput() throws IOException {
        return getStationPairsOutput(OUTPUT_PATH);
    }

    public List<Double> getStationPairsOutputTest() throws IOException {
        return getStationPairsOutput(TEST_OUTPUT_PATH);
    }

    public List<Double> getTransportationOutput() throws IOException {
        return getTransportationOutput(OUTPUT_PATH);

    }

    public List<Double> getTransportationOutputTest() throws IOException {
        return getTransportationOutput(TEST_OUTPUT_PATH);
    }

    private List<Double> getShipmentsOutput(String path) throws IOException {
        return Utils.stringToDouble(readFile(path + ORDERS_PRICE_FILE_NAME));
    }

    private List<Integer> getDateIntervalsOutput(String path) throws IOException {
        return Utils.stringToInteger(readFile(path + DATES_FILE_NAME));
    }

    private List<String> getHourIntervalsOutput(String path) throws IOException {
        return readFile(path + WORKING_HOURS_FILE_NAME);
    }

    private List<Integer> getUserPairsOutput(String path) throws IOException {
        return Utils.stringToInteger(readFile(path + NETWORK_FILE_NAME));
    }

    private List<String> getCustomEmailsOutput(String path) throws IOException {
        return readFile(path + EMAILS_FILE_NAME);
    }

    private List<Integer> getShipCodesOutput(String path) throws IOException {
        return Utils.stringToInteger(readFile(path + SHIP_CODES_FILE_NAME));

    }

    private List<Double> getStationPairsOutput(String path) throws IOException {
        return Utils.stringToDouble(readFile(path + STATIONS_FILE_NAME));

    }

    private List<Double> getTransportationOutput(String path) throws IOException {
        return Utils.stringToDouble(readFile(path + ORDERS_WEIGHT_FILE_NAME));
    }

    public void write(String fileName, List<String> lines) throws IOException{
        Files.write(Paths.get(RESOURCES_PATH + OUTPUT_PATH + fileName), lines, StandardCharsets.UTF_8);
    }
}
