package al.ship.core.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * @author aleksruci on 29/Apr/2019
 */
public class ResourceFileLayer extends DefaultIOLayer {
    private static final String FILES_PATH = RESOURCES_PATH + "";

    private static final String HOLIDAYS_FILE_NAME = "holidays.txt";
    private static final String WORKING_SATURDAYS_FILE_NAME = "working_saturdays.txt";

    public List<Calendar> getHolidays() throws IOException {
        return getGregorianCalendars(HOLIDAYS_FILE_NAME);
    }

    public List<Calendar> getWorkingSaturdays() throws IOException {
        return getGregorianCalendars(WORKING_SATURDAYS_FILE_NAME);
    }

    private List<Calendar> getGregorianCalendars(String workingSaturdaysFileName) throws IOException {
        List<String> lines = readFile(FILES_PATH + workingSaturdaysFileName);

        List<Calendar> workingSaturdays = new ArrayList<>();
        for(String line: lines) {
            String[] dates = line.split(" ");
            workingSaturdays.add(new GregorianCalendar(Integer.valueOf(dates[2]), Integer.valueOf(dates[1]) - 1, Integer.valueOf(dates[0])));
        }
        return workingSaturdays;
    }
}
