package al.ship.core.io;

import al.ship.core.model.CustomEmail;
import al.ship.core.model.DateInterval;
import al.ship.core.model.driver.HoursInterval;
import al.ship.core.model.shipment.Order;
import al.ship.core.model.shipment.ShipCode;
import al.ship.core.model.shipment.Shipment;
import al.ship.core.model.transport.Road;
import al.ship.core.model.transport.Station;
import al.ship.core.model.transport.StationPair;
import al.ship.core.model.transport.Transportation;
import al.ship.core.model.user.UserPair;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author aleksruci on 28/Apr/2019
 */
public class InputFileLayer extends DefaultIOLayer {
    private static final String INPUT_PATH = RESOURCES_PATH + "input/";
    private static final String TEST_INPUT_PATH = RESOURCES_PATH + "input/test/";

    private static final String ORDERS_PRICE_FILE_NAME = "orders_price_input.txt";
    private static final String DATES_FILE_NAME = "dates_input.txt";
    private static final String WORKING_HOURS_FILE_NAME = "working_hours_input.txt";
    private static final String NETWORK_FILE_NAME = "network_input.txt";
    private static final String EMAILS_FILE_NAME = "email_input.txt";
    private static final String SHIP_CODES_FILE_NAME = "ship_codes_input.txt";
    private static final String STATIONS_FILE_NAME = "stations_input.txt";
    private static final String ORDERS_WEIGHT_FILE_NAME = "orders_weight_input.txt";

    public List<Shipment> getShipments() throws IOException {
        return getShipments(INPUT_PATH);
    }

    public List<Shipment> getShipmentsTest() throws IOException {
        return getShipments(TEST_INPUT_PATH);
    }

    public List<DateInterval> getDateIntervals() throws IOException, ParseException {
        return getDateIntervals(INPUT_PATH);
    }

    public List<DateInterval> getDateIntervalsTest() throws IOException, ParseException {
        return getDateIntervals(TEST_INPUT_PATH);
    }

    public List<HoursInterval> getHourIntervals() throws IOException {
        return getHourIntervals(INPUT_PATH);
    }

    public List<HoursInterval> getHourIntervalsTest() throws IOException {
        return getHourIntervals(TEST_INPUT_PATH);
    }

    public List<List<UserPair>> getUserPairs() throws IOException {
        return getUserPairs(INPUT_PATH);
    }

    public List<List<UserPair>> getUserPairsTest() throws IOException {
        return getUserPairs(TEST_INPUT_PATH);
    }

    public List<CustomEmail> getCustomEmails() throws IOException {
        return getCustomEmails(INPUT_PATH);
    }

    public List<CustomEmail> getCustomEmailsTest() throws IOException {
        return getCustomEmails(TEST_INPUT_PATH);
    }

    public List<ShipCode> getShipCodes() throws IOException {
        return getShipCodes(INPUT_PATH);
    }

    public List<ShipCode> getShipCodesTest() throws IOException {
        return getShipCodes(TEST_INPUT_PATH);
    }

    public List<StationPair> getStationPairs() throws IOException {
        return getStationPairs(INPUT_PATH);
    }

    public List<StationPair> getStationPairsTest() throws IOException {
        return getStationPairs(TEST_INPUT_PATH);
    }

    public List<Transportation> getTransportation() throws IOException {
        return getTransportation(INPUT_PATH);
    }

    public List<Transportation> getTransportationTest() throws IOException {
        return getTransportation(TEST_INPUT_PATH);
    }

    private List<Shipment> getShipments(String path) throws IOException {
        List<String> lines = readFile(path + ORDERS_PRICE_FILE_NAME);

        List<Shipment> shipments = new ArrayList<>();
        for (String line : lines) {
            List<Order> orders = new ArrayList<>();
            for (String s : line.split(" ")) {
                Order order = new Order();
                order.setPrice(Double.valueOf(s));
                orders.add(order);
            }
            shipments.add(new Shipment(orders));
        }
        return shipments;
    }

    private List<DateInterval> getDateIntervals(String path) throws IOException, ParseException {
        List<String> lines = readFile(path + DATES_FILE_NAME);

        List<DateInterval> dateIntervals = new ArrayList<>();
        for (String line : lines) {
            String[] dates = line.split(" ");
            dateIntervals.add(new DateInterval(dates[0], dates[1]));
        }
        return dateIntervals;
    }

    private List<HoursInterval> getHourIntervals(String path) throws IOException {
        List<String> lines = readFile(path + WORKING_HOURS_FILE_NAME);

        List<HoursInterval> hourIntervals = new ArrayList<>();
        for (String line : lines) {
            String[] driver_times = line.split(" ");
            hourIntervals.add(new HoursInterval(Integer.valueOf(driver_times[0]), driver_times[1], driver_times[2], driver_times[3]));
        }
        return hourIntervals;
    }

    private List<List<UserPair>> getUserPairs(String path) throws IOException {
        List<String> lines = readFile(path + NETWORK_FILE_NAME);

        List<List<UserPair>> pairsMatrix = new ArrayList<>();
        int i = 0;
        while (i < lines.size()) {
            int nrPairs = Integer.valueOf(lines.get(i).split(" ")[0]);
            List<UserPair> pairs = new ArrayList<>();
            for (int j = 1; j <= nrPairs; j++) {
                String[] row = lines.get(i + j).split(" ");
                pairs.add(new UserPair(Integer.valueOf(row[0]), Integer.valueOf(row[1])));
            }
            pairsMatrix.add(pairs);
            i += nrPairs + 1;
        }
        return pairsMatrix;
    }

    private List<CustomEmail> getCustomEmails(String path) throws IOException {
        List<String> lines = readFile(path + EMAILS_FILE_NAME);

        List<CustomEmail> emails = new ArrayList<>();
        int i = 0;
        while (i < lines.size()) {
            CustomEmail email = new CustomEmail();

            int cnt = Integer.valueOf(lines.get(i));
            for (int j = 1; j <= cnt; j++) {
                String[] tags = lines.get(i + j).split(" ");
                StringBuilder tag = new StringBuilder();
                for (int k = 1; k < tags.length; k++) {
                    tag.append(tags[k]);
                    if (k != tags.length - 1) {
                        tag.append(" ");
                    }
                }
                email.putTag(tags[0].substring(1), tag.toString());
            }
            email.setTemplate(lines.get(i + cnt + 1));
            i += cnt + 2;

            emails.add(email);
        }
        return emails;
    }

    private List<ShipCode> getShipCodes(String path) throws IOException {
        List<String> lines = readFile(path + SHIP_CODES_FILE_NAME);

        List<ShipCode> codes = new ArrayList<>();
        int i = 0;
        while (i < lines.size()) {
            String[] dims = lines.get(i).split(" "); // dimensions of matrix
            int nrRows = Integer.valueOf(dims[0]);
            int nrCols = Integer.valueOf(dims[1]);
            int[][] matrix = new int[nrRows][nrCols];

            for (int j = 0; j < nrRows; j++) {
                String[] row = lines.get(i + j + 1).split(" ");
                for (int k = 0; k < nrCols; k++) {
                    matrix[j][k] = Integer.valueOf(row[k]);
                }
            }
            codes.add(new ShipCode(matrix));
            i += nrRows + 1;
        }
        return codes;
    }

    private List<StationPair> getStationPairs(String path) throws IOException {
        List<String> lines = readFile(path + STATIONS_FILE_NAME);

        List<StationPair> pairs = new ArrayList<>();
        int i = 0;
        while (i < lines.size()) {
            String[] data = lines.get(i).split(" ");
            int nrStations = Integer.valueOf(data[0]);
            int nrConnection = Integer.valueOf(data[1]);
            Map<Integer, Station> stations = new HashMap<>();

            for (int j = 0; j < nrStations; j++) {
                stations.put(j + 1, new Station(j + 1));
            }

            for (int j = 1; j <= nrConnection; j++) {
                String[] row = lines.get(i + j).split(" ");
                Station source = stations.get(Integer.valueOf(row[0]));
                Station destination = stations.get(Integer.valueOf(row[1]));
                source.addNeighbour(new Road(Double.valueOf(row[2]), destination, source));
            }
            i += nrConnection + 1;
            String[] row = lines.get(i).split(" ");
            Station source = stations.get(Integer.valueOf(row[0]));
            Station destination = stations.get(Integer.valueOf(row[1]));
            pairs.add(new StationPair(source, destination));
            i++;
        }
        return pairs;
    }

    private List<Transportation> getTransportation(String path) throws IOException {
        List<String> lines = readFile(path + ORDERS_WEIGHT_FILE_NAME);

        List<Transportation> transportations = new ArrayList<>();
        for (int i = 0; i < lines.size(); i += 2) {
            String[] ships = lines.get(i).split(" ");
            List<Order> orders = new ArrayList<>();
            for (String s : lines.get(i + 1).split(" ")) {
                Order order = new Order();
                order.setWeight(Double.valueOf(s));
                orders.add(order);
            }
            transportations.add(new Transportation(Integer.valueOf(ships[0]), Integer.valueOf(ships[1]), orders));
        }
        return transportations;
    }

}
