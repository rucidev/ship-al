package al.ship.core.algorithm;

import al.ship.core.model.shipment.Order;
import al.ship.core.model.transport.*;

import java.util.List;
import java.util.PriorityQueue;

/**
 * @author aleksruci on 28/Apr/2019
 */
public class TransportAlgorithms {
    /**
     * @param transportation ship capacities and orders to transport
     * @return max possible weight that we can allocate
     */
    public static double getMaxWeightInShips(Transportation transportation) {
        return getMaxWeightInShips(transportation, 0, 0, 0);
    }


    /**
     * @param transportation ship capacities and orders to transport
     * @param allocated1 amount currenly occupied in the first ship
     * @param allocated2 amount currently occupied in the second ship
     * @return max possible weight that we can allocate
     */
    private static double getMaxWeightInShips(Transportation transportation, int i, double allocated1, double allocated2) {
        double ship1 = transportation.getShip1Capacity();
        double ship2 = transportation.getShip2Capacity();
        List<Order> orders = transportation.getOrders();

        if (allocated1 == ship1 && allocated2 == ship2) {
            return ship1 + ship2;
        }

        double max = allocated1 + allocated2;

        if (i < orders.size()) {
            Order order = orders.get(i);
            if (allocated1 + order.getWeight() <= ship1) {
                max = Math.max(getMaxWeightInShips(transportation, i + 1, allocated1 + order.getWeight(), allocated2), max);
            }
            if (allocated2 + order.getWeight() <= ship2) {
                max = Math.max(getMaxWeightInShips(transportation, i + 1, allocated1, allocated2 + order.getWeight()), max);
            }
            max = Math.max(getMaxWeightInShips(transportation, i + 1, allocated1, allocated2), max);
        }
        return max;
    }

    public static double getShortestPathLength(Station start, Station end) {
        start.setDistance(0);
        PriorityQueue<Station> heap = new PriorityQueue<>();
        heap.add(start);
        while (!heap.isEmpty()) {
            Station station = heap.poll();
            for (Road road : station.getAdjRoads()) {
                Station neighbour = road.getTarget();
                double distance = station.getDistance() + road.getCost();
                if (distance < neighbour.getDistance()) {
                    heap.remove(neighbour);
                    neighbour.setDistance(distance);
                    neighbour.setPredecessor(station);
                    heap.add(neighbour);
                }
                if(neighbour.getId() == end.getId()) {
                    return neighbour.getDistance();
                }
            }
        }
        return -1;
    }

}
