package al.ship.core.algorithm;

import al.ship.core.model.CustomEmail;

/**
 * @author aleksruci on 27/Apr/2019
 */
public class EmailAlgorithms {
    public static final char KEYWORD_IDENTIFIER = '#';

    /**
     * @param customEmail email including keywords to be replaced by tags
     * @return generated email including tags instead of keywords
     * Every keyword in the template is identified by KEYWORD IDENTIFIER
     * # in front of it. #email should be replaced by the value of the key
     * email in the map
     */
    public static String getEmailFromTemplate(CustomEmail customEmail) {
        StringBuilder email = new StringBuilder();
        StringBuilder keyword = new StringBuilder();

        String template = customEmail.getTemplate();
        for (int i = 0, n = template.length(); i < n; i++) {
            char c = template.charAt(i);

            if (c == KEYWORD_IDENTIFIER) {
                c = template.charAt(++i);
                while (i < n - 1 && isEndOfWord(c)) { // find the keyword
                    keyword.append(c);
                    c = template.charAt(++i);
                }
                email.append(customEmail.getTags().get(keyword.toString()));
                keyword.setLength(0);
            }

            email.append(c);
        }
        return email.toString();
    }

    private static boolean isEndOfWord(char c) {
        return c != ' ' && c != ',' && c != '.' && c != '!' && c != '?' && c != ';' && c != ':' && c != '\n';
    }
}
