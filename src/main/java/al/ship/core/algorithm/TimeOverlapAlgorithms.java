package al.ship.core.algorithm;

import al.ship.core.model.driver.DriverWorkingHours;
import al.ship.core.model.driver.HoursInterval;

import java.util.*;

/**
 * @author aleksruci on 9/19/2018
 */
public class TimeOverlapAlgorithms {
    //Runs in O(nlogn) time complexity assuming DayOfWeek is unique throughout the list

    public static boolean hasOverlap(HoursInterval newInterval, DriverWorkingHours workingHours) {
        List<HoursInterval> existingIntervals = workingHours.getTimeIntervalsByDayOfWeekList(newInterval.getDayOfWeek());
        return hasOverlap(newInterval, existingIntervals);
    }

    private static boolean hasOverlap(HoursInterval newInterval, List<HoursInterval> hoursIntervals) {
        for (int i = 0; i < hoursIntervals.size(); i++) {
            if (newInterval.hasOverlap(hoursIntervals.get(i))) {
                return true;
            }
        }
        return false;
    }

}