package al.ship.core.algorithm;

import al.ship.core.model.discount.Discount;
import al.ship.core.model.discount.DiscountType;
import al.ship.core.model.shipment.Order;
import al.ship.core.model.shipment.OrderPriceComparator;
import al.ship.core.model.shipment.Shipment;
import java.util.List;

/**
 * @author aleksruci on 27/Apr/2019
 */
public class DiscountAlgorithms {

    /**
     * @param shipment
     * Applies "Order 3, Pay 2" discount to the shipment
     * thus decreasing its price
     */
    public static void applyBestOrder3Pay2Deal(Shipment shipment) {
        Discount discount = new Discount(DiscountType.ORDER3PAY2);
        List<Order> orders = shipment.getOrders();
        int n = orders.size();

        orders.sort(new OrderPriceComparator());
        for(int i = n - 1; i >= 0; i--) {
            if((n - i) % 3 == 0) {
                discount.add(orders.get(i).getPrice());
            }
        }
        shipment.setDiscount(discount);
    }
}
