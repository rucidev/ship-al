package al.ship.core.algorithm;

import al.ship.core.model.user.User;

import java.util.HashMap;
import java.util.Map;

/**
 * @author aleksruci on 11/Mär/2019
 *
 * User network of networks
 */
public class UserNetwork {
    private Map<Integer, Vertex> users;
    private int max; // largest connection length

    public UserNetwork() {
        this.users = new HashMap<>();
        this.max = Integer.MIN_VALUE;
    }

    public void union(User u1, User u2) {
        int root1 = root(u1);
        int root2 = root(u2);

        if(root1 == root2) {
            return;
        }

        if(users.get(root1).size < users.get(root2).size) {
            users.get(root1).parent = root2;
            users.get(root2).size += users.get(root1).size;
            if(users.get(root2).size > max) {
                max = users.get(root2).size;
            }
        } else {
            users.get(root2).parent = root1;
            users.get(root1).size += users.get(root2).size;
            if(users.get(root1).size > max) {
                max = users.get(root1).size;
            }
        }
    }

    private int root(User user) {
        addUserIfNecessary(user);

        int id = user.getId();
        while(users.get(id).parent != id) {
            users.get(id).parent = users.get(users.get(id).parent).parent; // to make receiving root amortized O(1)
            id = users.get(id).parent;
        }
        return id;
    }

    private void addUserIfNecessary(User user) {
        if(users.get(user.getId()) == null) {
            users.put(user.getId(), new Vertex(user));
        }
    }

    public int getMaxConnectionLength() {
        return max;
    }

    // TODO 11/05/2019 write a method to identify if 2 users are connected
    public boolean isConnected(User u1, User u2) {
        return root(u1) == root(u2);
    }

    private class Vertex {
        private int parent;
        private User user;
        private int size;

        private Vertex(User user){
            this.parent = user.getId();
            this.user = user;
            this.size = 1;
        }

        private Vertex(int parent, User user){
            this.parent = parent;
            this.user = user;
            this.size = 1;
        }
    }
}
