package al.ship.core.algorithm;

import java.util.*;

/**
 * @author aleksruci on 27/Apr/2019
 */
public class WorkingDaysAlgorithm {
    /* TODO 11/05/2019 we should use another data structure instead of List to store
        these data in order to access them in constant time: O(1) */
    private List<Calendar> publicHolidaysList;
    private List<Calendar> workingDaysList;

    private Set<Calendar> publicHolidays;
    private Set<Calendar> workingDays;

    public WorkingDaysAlgorithm(List<Calendar> publicHolidaysList, List<Calendar> workingDaysList) {
        this.publicHolidays = getHashSetFromCalendar(publicHolidaysList);
        this.workingDays = getHashSetFromCalendar(workingDaysList);
    }

    public int getNumberOfWorkingDays(Date startDate, Date endDate) {
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(startDate);

        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(endDate);

        int noWorkDays = 0;

        if (startCalendar.getTimeInMillis() == endCalendar.getTimeInMillis()) {
            return 0;
        }

        if (startCalendar.getTimeInMillis() > endCalendar.getTimeInMillis()) {
            startCalendar.setTime(endDate);
            endCalendar.setTime(startDate);
        }

        do {
            if (!isSaturday(startCalendar) && !isSunday(startCalendar) && !isPublicHoliday(startCalendar)) {
                ++noWorkDays;
            }
            if (isWorkingSaturday(startCalendar)) {
                ++noWorkDays;
            }
            startCalendar.add(java.util.Calendar.DAY_OF_MONTH, 1);
        } while (startCalendar.getTimeInMillis() <= endCalendar.getTimeInMillis());

        return noWorkDays;
    }

    private boolean isSaturday(Calendar calendar) {
        return isGivenDay(calendar, Calendar.SATURDAY);
    }

    private boolean isSunday(Calendar calendar) {
        return isGivenDay(calendar, Calendar.SUNDAY);
    }

    private boolean isPublicHoliday(Calendar calendar) {
        return isSpecialDay(calendar, publicHolidays);
    }

    private boolean isWorkingSaturday(Calendar calendar) {
        return isSpecialDay(calendar, workingDays);
    }

    private boolean isGivenDay(Calendar calendar, int day) {
        return calendar.get(Calendar.DAY_OF_WEEK) == day;
    }

    private boolean isSpecialDay(Calendar date, Set<Calendar> calendars) {
        int day = date.get(Calendar.DAY_OF_MONTH);
        int month = date.get(Calendar.MONTH);
        int year = date.get(Calendar.YEAR);

        Calendar calendar = new GregorianCalendar(year, month, day);
        return calendars.contains(calendar);
    }

    private Set<Calendar> getHashSetFromCalendar(List<Calendar> calendars) {
        return new HashSet<>(calendars);
    }
}

