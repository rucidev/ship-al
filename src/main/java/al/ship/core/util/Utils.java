package al.ship.core.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author aleksruci on 29/Apr/2019
 */
public class Utils {

    public static List<Double> stringToDouble(List<String> arr) {
        List<Double> res = new ArrayList<>(arr.size());
        for (String s : arr) {
            res.add(Double.valueOf(s));
        }
        return res;
    }

    public static List<Integer> stringToInteger(List<String> arr) {
        List<Integer> res = new ArrayList<>(arr.size());
        for (String s : arr) {
            res.add(Integer.valueOf(s));
        }
        return res;
    }
}
