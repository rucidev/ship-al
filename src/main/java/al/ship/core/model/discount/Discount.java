package al.ship.core.model.discount;

/**
 * @author aleksruci on 27/Apr/2019
 */
public class Discount {
    private double value;
    private DiscountType type;
    private String description;

    public Discount(DiscountType type) {
        this.type = type;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void add(double amount) {
        this.value += amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DiscountType getType() {
        return type;
    }

    public void setType(DiscountType type) {
        this.type = type;
    }
}
