package al.ship.core.model.discount;

/**
 * @author aleksruci on 27/Apr/2019
 */
public enum DiscountType {
    ORDER3PAY2(1, "Order 3, Pay 3");

    private int id;
    private String description;

    DiscountType(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
