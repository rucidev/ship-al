package al.ship.core.model.driver;

/**
 * @author aleksruci on 9/20/2018
 */
public class HoursInterval implements Comparable<HoursInterval> {
    private int id;
    private String dayOfWeek;
    private String startTime;
    private String endTime;

    public HoursInterval(){

    }

    public HoursInterval(int id){
        this.id = id;
    }

    public HoursInterval(String dayOfWeek, String startTime, String endTime) {
        this.dayOfWeek = dayOfWeek;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public HoursInterval(int id, String dayOfWeek, String startTime, String endTime) {
        this(dayOfWeek, startTime, endTime);
        this.id = id;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public int compareTo(HoursInterval hoursInterval) {
        int startTime1 = Integer.parseInt(this.getStartTime().substring(0, 2) + this.getStartTime().substring(3, 5));
        int startTime2 = Integer.parseInt(hoursInterval.getStartTime().substring(0, 2) + hoursInterval.getStartTime().substring(3, 5));

        return startTime1 > startTime2 ? 1 : -1;
    }

    //Checks for interval overlap and within interval if StartTime is after EndTime(overlap within interval)
    public boolean hasOverlap(HoursInterval hoursInterval) {
        String timeIntervalStartTime = hoursInterval.getStartTime();
        String timeIntervalEndTime = adjustEndTime(hoursInterval.getEndTime());
        String thisStartTime = this.getStartTime();
        String thisEndTime = adjustEndTime(this.getEndTime());

        return (isAfter(timeIntervalEndTime, thisStartTime) && isAfter(thisEndTime, timeIntervalStartTime))
                || isAfter(thisStartTime, thisEndTime)
                || isAfter(timeIntervalStartTime, timeIntervalEndTime)
                || (thisStartTime.equals(timeIntervalStartTime) && thisEndTime.equals(timeIntervalEndTime));
    }

    private boolean isAfter(String time1, String time2) {
        int t1 = Integer.parseInt(time1.substring(0, 2) + time1.substring(3, 5));
        int t2 = Integer.parseInt(time2.substring(0, 2) + time2.substring(3, 5));
        return t1 > t2;
    }

    private String adjustEndTime(String timeIntervalEndTime) {
        return timeIntervalEndTime.equals("00:00") ? "24:00" : timeIntervalEndTime;
    }

    @Override
    public String toString() {
        return "HoursInterval{" +
                "id=" + id +
                ", dayOfWeek='" + dayOfWeek + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                '}';
    }
}