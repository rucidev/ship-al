package al.ship.core.model.driver;

import java.util.*;

/**
 * @author aleksruci on 9/17/2018
 */
public class DriverWorkingHours {
    private int driverId;
    private Map<String, ArrayList<HoursInterval>> timeIntervalsByDayOfWeek;

    public DriverWorkingHours(){
        timeIntervalsByDayOfWeek = new HashMap<>();
    }

    public DriverWorkingHours(int driverId) {
        this();
        this.driverId = driverId;
    }

    public int getDriverId() {
        return driverId;
    }

    public void setDriverId(int driverId) {
        this.driverId = driverId;
    }

    public void addTimeInterval(String dayOfWeek, HoursInterval hoursInterval){
        hoursInterval.setDayOfWeek(dayOfWeek);
        addTimeInterval(hoursInterval);
    }

    public void addTimeInterval(HoursInterval hoursInterval){
        if(timeIntervalsByDayOfWeek.get(hoursInterval.getDayOfWeek()) == null){
            setTimeIntervalsByDayOfWeekList(hoursInterval.getDayOfWeek(), new ArrayList<>());
        }
        timeIntervalsByDayOfWeek.get(hoursInterval.getDayOfWeek()).add(hoursInterval);
    }

    public List<HoursInterval> getTimeIntervalsByDayOfWeekList(String dayOfWeek) {
        return timeIntervalsByDayOfWeek.get(dayOfWeek);
    }

    public void setTimeIntervalsByDayOfWeekList(String dayOfWeek, ArrayList<HoursInterval> hoursIntervals) {
        timeIntervalsByDayOfWeek.put(dayOfWeek, hoursIntervals);
    }
}
