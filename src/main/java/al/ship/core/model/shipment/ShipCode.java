package al.ship.core.model.shipment;

/**
 * @author aleksruci on 27/Apr/2019
 */
public class ShipCode {
    private static final int WHITE = 0;
    private static final int BLACK = 1;

    private int[][] code;
    private int[][] visited;

    public ShipCode(int[][] code){
        this.code = code;
        this.visited = new int[code.length][code[0].length];
    }

    public int getOrderId() {
        int dimSum = 0;
        int nrRect = 0;

        for (int i = 0; i < code.length; i++) {
            for (int j = 0; j < code[i].length; j++) {
                if (!isVisited(i, j) && isRect(i, j)) {
                    dimSum += Math.sqrt(getNrCellsInRectangle(i, j));
                    nrRect++;
                }
            }
        }
        return dimSum * nrRect;
    }

    private int getNrCellsInRectangle(int i, int j) {
        if (!isVisitable(i, j)) {
            return 0;
        }

        int nrCells = 1;
        visited[i][j] = BLACK;
        nrCells += getNrCellsInRectangle(i + 1, j); // GO DOWN
        nrCells += getNrCellsInRectangle(i, j + 1); // GO RIGHT
        nrCells += getNrCellsInRectangle(i - 1, j); // GO UP
        nrCells += getNrCellsInRectangle(i, j - 1); // GO LEFT
        return nrCells;
    }

    private boolean isOutOfBound(int i, int j) {
        return (i >= code.length) || (i < 0) || (j >= code[i].length) || (j < 0);
    }

    private boolean isVisited(int i, int j) {
        return visited[i][j] == BLACK;
    }

    private boolean isRect(int i, int j) {
        return code[i][j] == BLACK;
    }

    private boolean isVisitable(int i, int j) {
        return !isOutOfBound(i, j) && code[i][j] != WHITE && visited[i][j] != BLACK;
    }

    public int[][] getCode() {
        return code;
    }

    public void setCode(int[][] code) {
        this.code = code;
    }
}
