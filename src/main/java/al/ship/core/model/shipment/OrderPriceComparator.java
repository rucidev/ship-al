package al.ship.core.model.shipment;

import java.util.Comparator;

/**
 * @author aleksruci on 28/Apr/2019
 */

//TODO 11/05/2019 create a comparator to compare orders by price and make use of it

public class OrderPriceComparator implements Comparator<Order> {

    @Override
    public int compare(Order order1, Order order2) {
        return Double.compare(order1.getPrice(), order2.getPrice());
    }
}
