package al.ship.core.model.shipment;

import al.ship.core.model.discount.Discount;

import java.util.List;

/**
 * @author aleksruci on 27/Apr/2019
 */
public class Shipment {
    private int id;
    private List<Order> orders;
    private Discount discount;
    private double value;
    private ShipCode shipCode;

    public Shipment(List<Order> orders) {
        this.orders = orders;
        for(Order order: orders) {
            this.value += order.getPrice();
        }
    }

    public Shipment(int id, List<Order> orders) {
        this(orders);
        this.id = id;
    }

    public double getPrice() {
        return this.value - (discount != null ? discount.getValue() : 0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public boolean addOrder(Order order) {
        this.value += order.getPrice();
        return orders.add(order);
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public ShipCode getShipCode() {
        return shipCode;
    }

    public void setShipCode(ShipCode shipCode) {
        this.shipCode = shipCode;
    }
}
