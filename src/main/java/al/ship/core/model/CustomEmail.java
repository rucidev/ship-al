package al.ship.core.model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author aleksruci on 28/Apr/2019
 */
public class CustomEmail {
    private String template;
    private Map<String, String> tags;

    public CustomEmail() {
        this.tags = new HashMap<>();
    }

    public void putTag(String key, String value) {
        this.tags.put(key, value);
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public Map<String, String> getTags() {
        return tags;
    }

    public void setTags(Map<String, String> tags) {
        this.tags = tags;
    }
}
