package al.ship.core.model.transport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author aleksruci on 28/Apr/2019
 */
public class Station implements Comparable<Station> {
    private int id;
    private double distance;
    private List<Road> adjRoads;
    private Station predecessor;

    public Station(int id) {
        this.id = id;
        this.adjRoads = new ArrayList<>();
        this.distance = Double.MAX_VALUE;
    }

    @Override
    public int compareTo(Station station) {
        return Double.compare(this.distance, station.getDistance());
    }

    public Station getPredecessor() {
        return predecessor;
    }

    public void setPredecessor(Station predecessor) {
        this.predecessor = predecessor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public void addNeighbour(Road road) {
        adjRoads.add(road);
    }

    public List<Road> getAdjRoads() {
        return adjRoads;
    }

    public void setAdjRoads(List<Road> adjRoads) {
        this.adjRoads = adjRoads;
    }

    public String toString() {
        return id + "";
    }

}
