package al.ship.core.model.transport;

import al.ship.core.model.shipment.Order;

import java.util.List;

/**
 * @author aleksruci on 30/Apr/2019
 */
public class Transportation {
    private double ship1Capacity;
    private double ship2Capacity;
    private List<Order> orders;

    public Transportation(double ship1Capacity, double ship2Capacity, List<Order> orders) {
        this.ship1Capacity = ship1Capacity;
        this.ship2Capacity = ship2Capacity;
        this.orders = orders;
    }

    public Transportation() {
    }

    public double getShip1Capacity() {
        return ship1Capacity;
    }

    public void setShip1Capacity(double ship1Capacity) {
        this.ship1Capacity = ship1Capacity;
    }

    public double getShip2Capacity() {
        return ship2Capacity;
    }

    public void setShip2Capacity(double ship2Capacity) {
        this.ship2Capacity = ship2Capacity;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
