package al.ship.core.model.transport;

/**
 * @author aleksruci on 29/Apr/2019
 */
public class StationPair {
    private Station start;
    private Station end;

    public StationPair(Station start, Station end) {
        this.start = start;
        this.end = end;
    }

    public Station getStart() {
        return start;
    }

    public void setStart(Station start) {
        this.start = start;
    }

    public Station getEnd() {
        return end;
    }

    public void setEnd(Station end) {
        this.end = end;
    }
}
