package al.ship.core.model.transport;

/**
 * @author aleksruci on 28/Apr/2019
 */
public class Road {
    private double cost;
    private Station target;
    private Station source;

    public Road(double cost, Station target, Station source) {
        this.cost = cost;
        this.target = target;
        this.source = source;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public Station getTarget() {
        return target;
    }

    public void setTarget(Station target) {
        this.target = target;
    }

    public Station getSource() {
        return source;
    }

    public void setSource(Station source) {
        this.source = source;
    }


}

