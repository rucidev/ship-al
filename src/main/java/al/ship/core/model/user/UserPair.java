package al.ship.core.model.user;

/**
 * @author aleksruci on 28/Apr/2019
 */
public class UserPair {
    private User user1;
    private User user2;

    public UserPair(int id1, int id2) {
        this.user1 = new User(id1);
        this.user2 = new User(id2);
    }

    public UserPair(User user1, User user2) {
        this.user1 = user1;
        this.user2 = user2;
    }


    public User getUser1() {
        return user1;
    }

    public void setUser1(User user1) {
        this.user1 = user1;
    }

    public User getUser2() {
        return user2;
    }

    public void setUser2(User user2) {
        this.user2 = user2;
    }
}
