package al.ship.core.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author aleksruci on 28/Apr/2019
 */
public class DateInterval {
    private Date start;
    private Date end;

    public DateInterval(String start, String end) throws ParseException {
        this.start = new Date();
        this.start.setTime((new SimpleDateFormat("dd-MM-yyyy")).parse(start).getTime());

        this.end = new Date();
        this.end.setTime((new SimpleDateFormat("dd-MM-yyyy")).parse(end).getTime());
    }

    public DateInterval(Date start, Date end) {
        this.start = start;
        this.end = end;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
